#ifndef MY_CORE_HPP
#define MY_CORE_HPP

#include <vector>
#include <opencv2/core.hpp>

#include "utility.hpp"

namespace my
{
    void scale_tip(Subdiv2D& morpher, float factor);
    void scale_eyes(Subdiv2D& morpher, float factor);
    void scale_lips(Subdiv2D& morpher, float factor);
    void scale_face(Subdiv2D& morpher, float factor);
    void scale_nose(Subdiv2D& morpher, float factor);
    void scale_brow(Subdiv2D& morpher, float factor);
    void scale_jaw(Subdiv2D& morpher, float factor);

    void rotate_tip(Subdiv2D& morpher, float rad);
    void rotate_eyes(Subdiv2D& morpher, float rad);
    void rotate_lips(Subdiv2D& morpher, float rad);
    void rotate_face(Subdiv2D& morpher, float rad);
    void rotate_nose(Subdiv2D& morpher, float rad);
    void rotate_brow(Subdiv2D& morpher, float rad);
    void rotate_jaw(Subdiv2D& morpher, float rad);

    void translate_tip(Subdiv2D& morpher, float xfactor, float yfactor);
    void translate_eyes(Subdiv2D& morpher, float xfactor, float yfactor);
    void translate_lips(Subdiv2D& morpher, float xfactor, float yfactor);
    void translate_face(Subdiv2D& morpher, float xfactor, float yfactor);
    void translate_nose(Subdiv2D& morpher, float xfactor, float yfactor);
    void translate_brow(Subdiv2D& morpher, float xfactor, float yfactor);
    void translate_jaw(Subdiv2D& morpher, float xfactor, float yfactor);

    void scale(int begin, int end, Subdiv2D& morpher, float factor);
    void scale(int begin, int end, cv::Point2f center, Subdiv2D& morpher, float factor);
    void rotate(int begin, int end, Subdiv2D& morpher, float rad);
    void translate(int begin, int end, Subdiv2D& morpher, float xfactor, float yfactor);
}

#endif // MY_CORE_HPP
