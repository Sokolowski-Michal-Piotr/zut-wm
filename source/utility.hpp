#ifndef MY_UTILITY_HPP
#define MY_UTILITY_HPP

#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

namespace my
{
    class Subdiv2D : public cv::Subdiv2D
    {
    public:
        Subdiv2D(const cv::Rect& rect);
        cv::Point2f& point(const size_t id);
        void insert_margin(const cv::Mat& source);
        void getTriangleList(const cv::Mat& source, std::vector<cv::Vec6f>& list);
    };

    cv::Mat merge(const cv::Mat& left, const cv::Mat& right);
    std::vector<cv::Point2f> vec2pts(const cv::Vec6f& vec);

    cv::Point2f center(const std::vector<cv::Point2f>& points);
    float orientation(const std::vector<cv::Point2f>& points);

    float rad2deg(const float rad);
    float deg2rad(const float deg);
    float magnitude(const cv::Point2f& vector);
    cv::Point2f normalize(const cv::Point2f& vector);
    float dot(const cv::Point2f& a, const cv::Point2f& b);
    float angle(const cv::Point2f& a, const cv::Point2f& b);
    cv::Point2f rotate(const cv::Point2f& vector, float rad);

    namespace landmarks
    {
        constexpr int jaw_begin = 0;
        constexpr int jaw_end = 17;
        constexpr int left_eye_brow_begin = 17;
        constexpr int left_eye_brow_end = 22;
        constexpr int right_eye_brow_begin = 22;
        constexpr int right_eye_brow_end = 27;
        constexpr int nose_begin = 27;
        constexpr int nose_end = 31;
        constexpr int tip_begin = 31;
        constexpr int tip_center = 33;
        constexpr int tip_end = 36;
        constexpr int left_eye_begin = 36;
        constexpr int left_eye_end = 42;
        constexpr int right_eye_begin = 42;
        constexpr int right_eye_end = 48;
        constexpr int lips_begin = 48;
        constexpr int lips_end = 68;
        constexpr int face_begin = 0;
        constexpr int face_end = 68;
    }
}

#endif // MY_UTILITY_HPP
