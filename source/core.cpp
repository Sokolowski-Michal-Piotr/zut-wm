#include "core.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>

#include "utility.hpp"

using namespace my::landmarks;

namespace my
{
    void translate(int begin, int end, Subdiv2D& morpher, float xfactor, float yfactor)
    {
        cv::Point2f min, max;
        for (int i = begin; i < end; i++)
        {
            min.x = cv::min(min.x, morpher.point(i).x);
            min.y = cv::min(min.y, morpher.point(i).y);
            max.x = cv::max(max.x, morpher.point(i).x);
            max.y = cv::max(max.y, morpher.point(i).y);
        }
        cv::Point2f size = max - min;
        size.x *= xfactor;
        size.y *= yfactor;
        for (int i = begin; i < end; i++)
        {
            morpher.point(i) += size;
        }
    }

    void rotate(int begin, int end, cv::Point2f center, Subdiv2D& morpher, float rad)
    {
        for (int i = begin; i < end; i++)
        {
            cv::Point2f dir = morpher.point(i) - center;
            dir = my::rotate(dir, rad);
            morpher.point(i) = center + dir;
        }
    }

    void rotate(int begin, int end, Subdiv2D& morpher, float rad)
    {
        std::vector<cv::Point2f> part;
        for (int i = begin; i < end; i++)
            part.push_back(morpher.point(i));
        cv::Point2f center = my::center(part);
        rotate(begin, end, center, morpher, rad);
    }

    void scale(int begin, int end, cv::Point2f center, Subdiv2D& morpher, float factor)
    {
        for (int i = begin; i < end; i++)
        {
            cv::Point2f dir = morpher.point(i) - center;
            morpher.point(i) = center + factor * dir;
        }
    }

    void scale(int begin, int end, Subdiv2D& morpher, float factor)
    {
        std::vector<cv::Point2f> part;
        for (int i = begin; i < end; i++)
            part.push_back(morpher.point(i));
        cv::Point2f center = my::center(part);
        scale(begin, end, center, morpher, factor);
    }

    void scale_tip(Subdiv2D& morpher, float factor)
    {
        scale(tip_begin, tip_end, morpher.point(tip_center), morpher, factor);
    }

    void scale_eyes(Subdiv2D& morpher, float factor)
    {
        scale(left_eye_begin, left_eye_end, morpher, factor);
        scale(right_eye_begin, right_eye_end, morpher, factor);
    }

    void scale_lips(Subdiv2D& morpher, float factor)
    {
        scale(lips_begin, lips_end, morpher, factor);
    }

    void scale_face(Subdiv2D& morpher, float factor)
    {
        scale(face_begin, face_end, morpher, factor);
    }

    void scale_nose(Subdiv2D& morpher, float factor)
    {
        scale(nose_begin, nose_end, morpher, factor);
    }

    void scale_brow(Subdiv2D& morpher, float factor)
    {
        scale(left_eye_brow_begin, left_eye_brow_end, morpher, factor);
        scale(right_eye_brow_begin, right_eye_brow_end, morpher, factor);
    }

    void scale_jaw(Subdiv2D& morpher, float factor)
    {
        scale(jaw_begin, jaw_end, morpher, factor);
    }

    void translate_tip(Subdiv2D& morpher, float xfactor, float yfactor)
    {
        translate(tip_begin, tip_end, morpher, xfactor, yfactor);
    }

    void translate_eyes(Subdiv2D& morpher, float xfactor, float yfactor)
    {
        translate(left_eye_begin, left_eye_end, morpher, -xfactor, yfactor);
        translate(right_eye_begin, right_eye_end, morpher, xfactor, yfactor);
    }

    void translate_lips(Subdiv2D& morpher, float xfactor, float yfactor)
    {
        translate(lips_begin, lips_end, morpher, xfactor, yfactor);
    }

    void translate_face(Subdiv2D& morpher, float xfactor, float yfactor)
    {
        translate(face_begin, face_end, morpher, xfactor, yfactor);
    }

    void translate_nose(Subdiv2D& morpher, float xfactor, float yfactor)
    {
        translate(nose_begin, nose_end, morpher, xfactor, yfactor);
    }

    void translate_brow(Subdiv2D & morpher, float xfactor, float yfactor)
    {
        translate(left_eye_brow_begin, left_eye_brow_end, morpher, -xfactor, yfactor);
        translate(right_eye_brow_begin, right_eye_brow_end, morpher, xfactor, yfactor);
    }

    void translate_jaw(Subdiv2D & morpher, float xfactor, float yfactor)
    {
        translate(jaw_begin, jaw_end, morpher, xfactor, yfactor);
    }

    void rotate_tip(Subdiv2D& morpher, float rad)
    {
        rotate(tip_begin, tip_end, morpher, rad);
    }

    void rotate_eyes(Subdiv2D& morpher, float rad)
    {
        rotate(left_eye_begin, left_eye_end, morpher, -rad);
        rotate(right_eye_begin, right_eye_end, morpher, rad);
    }

    void rotate_lips(Subdiv2D& morpher, float rad)
    {
        rotate(lips_begin, lips_end, morpher, rad);
    }

    void rotate_face(Subdiv2D& morpher, float rad)
    {
        rotate(face_begin, face_end, morpher, rad);
    }

    void rotate_nose(Subdiv2D & morpher, float rad)
    {
        rotate(nose_begin, nose_end, morpher, rad);
    }

    void rotate_brow(Subdiv2D & morpher, float rad)
    {
        rotate(left_eye_brow_begin, left_eye_brow_end, morpher, -rad);
        rotate(right_eye_brow_begin, right_eye_brow_end, morpher, rad);
    }

    void rotate_jaw(Subdiv2D & morpher, float rad)
    {
        rotate(jaw_begin, jaw_end, morpher, rad);
    }
}
