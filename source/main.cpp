// Set of example images
// ../../../resources/BioID-FaceDatabase-V1.2/BioID_0002.pgm ../../../resources/BioID-FaceDatabase-V1.2/BioID_0006.pgm ../../../resources/BioID-FaceDatabase-V1.2/BioID_0012.pgm ../../../resources/BioID-FaceDatabase-V1.2/BioID_0016.pgm

#include <vector>
#include <string>
#include <cstdlib>
#include <iostream>
#include <exception>

#include <tclap/CmdLine.h>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <dlib/image_io.h>
#include <dlib/opencv/cv_image.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>

#include "core.hpp"
#include "utility.hpp"

int main(const int argc, const char * const argv[])
{
    try
    {
        std::string predictor_path;
        std::vector<std::string> image_paths;

        {
            TCLAP::CmdLine cmdline("WM Project - Michal Sokolowski");
            TCLAP::UnlabeledMultiArg<std::string> images("images", "Paths to images", true, "path");
            TCLAP::ValueArg<std::string> predictor("p", "predictor", "Path to predictor", false, 
                "../../../resources/shape_predictor_68_face_landmarks.dat", "path");
            cmdline.add(images);
            cmdline.add(predictor);
            cmdline.parse(argc, argv);
            image_paths = images.getValue();
            predictor_path = predictor.getValue();
        }

        dlib::shape_predictor predictor;
        dlib::frontal_face_detector detector;

        detector = dlib::get_frontal_face_detector();
        dlib::deserialize(predictor_path) >> predictor;

        for (const auto& path : image_paths)
        {
            cv::Mat source = cv::imread(path);

            dlib::array2d<dlib::rgb_pixel> image;
            dlib::assign_image(image, dlib::cv_image<dlib::rgb_pixel>(source));

            std::vector<dlib::rectangle> rects = detector(image);

            std::vector<cv::Point2f> points;
            for (const auto& rect : rects)
            {
                dlib::full_object_detection shape = predictor(image, rect);
                for (unsigned i = 0; i < shape.num_parts(); i++)
                    points.emplace_back(shape.part(i).x(), shape.part(i).y());
            }

            const cv::Rect rect(0, 0, source.cols, source.rows);
            my::Subdiv2D subdiv(rect);
            subdiv.insert(points);
            subdiv.insert_margin(source);
            my::Subdiv2D morpher = subdiv;

            my::scale_face(morpher, 0.9f);
            my::scale_tip(morpher, 0.8f);
            my::scale_eyes(morpher, 1.1f);
            my::scale_lips(morpher, 0.95f);
            my::scale_jaw(morpher, 1.04f);
            my::rotate_eyes(morpher, my::deg2rad(-7.0f));
            my::translate_lips(morpher, 0.0f, -0.015f);
            my::translate_eyes(morpher, 0.0f, 0.01f);
            my::translate_jaw(morpher, 0.0f, -0.01f);
            my::translate_face(morpher, 0.0f, -0.05f);

            cv::Mat result = cv::Mat::zeros(source.size(), source.type());
            std::vector<cv::Vec6f> tris_in, tris_out;
            subdiv.getTriangleList(source, tris_in);
            morpher.getTriangleList(source, tris_out);
            for (int i = 0; i < tris_in.size(); i++)
            {
                auto in = my::vec2pts(tris_in[i]);
                auto out = my::vec2pts(tris_out[i]);

                cv::Rect rin = cv::boundingRect(in);
                cv::Rect rout = cv::boundingRect(out);

                cv::Mat image;
                source(rin).copyTo(image);

                std::transform(in.begin(), in.end(), in.begin(), 
                    std::bind2nd(std::minus<cv::Point2f>(), cv::Point2f(rin.x, rin.y)));
                std::transform(out.begin(), out.end(), out.begin(), 
                    std::bind2nd(std::minus<cv::Point2f>(), cv::Point2f(rout.x, rout.y)));

                cv::Mat warped = cv::Mat::zeros(rout.size(), result.type());
                cv::Mat warpmat = cv::getAffineTransform(in, out);
                cv::warpAffine(image, warped, warpmat, rout.size(), cv::INTER_LINEAR, cv::BORDER_REPLICATE);

                cv::Mat mask = cv::Mat::zeros(rout.size(), CV_8U);
                cv::fillConvexPoly(mask, std::vector<cv::Point2i>(out.begin(), out.end()), cv::Scalar(255, 255, 255), CV_AA);

                warped.copyTo(result(rout), mask);
            }

            //result = source.clone();
            //for (const auto& t : tris_out)
            //{
            //    int width = 1;
            //    cv::Scalar color = { 255, 255, 255 };
            //    cv::line(result, cv::Point2f(t[0], t[1]), cv::Point2f(t[2], t[3]), color, width, CV_AA);
            //    cv::line(result, cv::Point2f(t[2], t[3]), cv::Point2f(t[4], t[5]), color, width, CV_AA);
            //    cv::line(result, cv::Point2f(t[4], t[5]), cv::Point2f(t[0], t[1]), color, width, CV_AA);
            //}
            //for (const auto& point : points)
            //    cv::circle(result, point, 2, cv::Scalar(255, 0, 255), -1);

            result = my::merge(source, result);
            cv::imshow("Result", result);
            size_t lastof = path.find_last_of('/');
            cv::setWindowTitle("Result", "Result: " + path.substr(lastof != std::string::npos ? lastof + 1 : 0));
            cv::waitKey();
        }

        return EXIT_SUCCESS;
    }
    catch (std::exception e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}
