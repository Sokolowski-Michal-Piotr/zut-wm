#include "utility.hpp"

#include <opencv2/imgproc.hpp>

namespace my
{
    Subdiv2D::Subdiv2D(const cv::Rect& rect)
        : cv::Subdiv2D(rect)
    {

    }

    cv::Point2f& Subdiv2D::point(const size_t id)
    {
        return vtx[id + 4].pt;
    }

    void Subdiv2D::getTriangleList(const cv::Mat& source, std::vector<cv::Vec6f>& list)
    {
        cv::Subdiv2D::getTriangleList(list);
        cv::Rect rect(0, 0, source.cols - 1.0f, source.rows - 1.0f);
        for (auto& t : list)
        {
            for (int i = 0; i < 6; i+=2)
            {
                float& x = t[i];
                float& y = t[i + 1];

                if (x < rect.x)
                    x = rect.x;
                else if (x > rect.x + rect.width)
                    x = rect.x + rect.width;

                if (y < rect.y)
                    y = rect.y;
                else if (y > rect.y + rect.height)
                    y = rect.y + rect.height;
            }
        }
        std::vector<cv::Vec6f>(list.begin() + 4, list.end() - 4).swap(list);
    }

    void Subdiv2D::insert_margin(const cv::Mat& source)
    {
        insert({ 0.0f, 0.0f });
        insert({ source.cols / 2.0f, 0.0f });
        insert({ source.cols - 1.0f, 0.0f });
        insert({ source.cols - 1.0f, source.rows / 2.0f });
        insert({ source.cols - 1.0f, source.rows - 1.0f });
        insert({ source.cols / 2.0f, source.rows - 1.0f });
        insert({ 0.0f, source.rows - 1.0f });
        insert({ 0.0f, source.rows / 2.0f });
    }

    cv::Mat merge(const cv::Mat& left, const cv::Mat& right)
    {
        cv::Mat result(cv::max(left.rows, right.rows), left.cols + right.cols, CV_8UC3);
        cv::Mat lroi(result, cv::Rect(0, 0, left.cols, left.rows));
        cv::Mat rroi(result, cv::Rect(left.cols, 0, right.cols, right.rows));
        left.copyTo(lroi);
        right.copyTo(rroi);
        return result;
    }

    std::vector<cv::Point2f> vec2pts(const cv::Vec6f& vec)
    {
        return { { vec[0], vec[1] }, { vec[2], vec[3] }, { vec[4], vec[5] } };
    }

    cv::Point2f center(const std::vector<cv::Point2f>& points)
    {
        auto m = cv::moments(points);
        return cv::Point2f(m.m10 / m.m00, m.m01 / m.m00);
    }

    float orientation(const std::vector<cv::Point2f>& points)
    {
        auto m = cv::moments(points);
        return 0.5f * atan2f(2 * m.mu11, m.mu20 - m.mu02);
    }

    float rad2deg(const float rad)
    {
        return rad * 180.0f / CV_PI;
    }

    float deg2rad(const float deg)
    {
        return deg * CV_PI / 180.0f;
    }

    float magnitude(const cv::Point2f& vector)
    {
        float x = vector.x;
        float y = vector.y;
        return sqrtf(x*x + y*y);
    }

    float dot(const cv::Point2f& a, const cv::Point2f& b)
    {
        return a.x * b.x + a.y * b.y;
    }

    float angle(const cv::Point2f& a, const cv::Point2f& b)
    {
        return acosf(my::dot(a, b));
    }

    cv::Point2f rotate(const cv::Point2f& vector, float rad)
    {
        float x = vector.x;
        float y = vector.y;
        float c = cosf(rad);
        float s = sinf(rad);
        return cv::Point2f(x*c - y*s, y*c + x*s);
    }

    cv::Point2f normalize(const cv::Point2f& vector)
    {
        return vector / magnitude(vector);
    }
}
