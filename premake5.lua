workspace 'wm_project'
    location 'build/%{_ACTION}'
    startproject 'wm_project'
    platforms { 'x86', 'x64' }
    configurations { 'debug', 'release' }
    targetdir 'bin/%{cfg.platform}/%{cfg.buildcfg}'

    filter 'debug'
        flags 'Symbols'
        defines { 'DEBUG' }
    filter 'release'
        optimize 'On'
        defines { 'NDEBUG' }
    filter 'action:vs*'
        buildoptions { '/wd4018 /wd4244 /wd4305' }
    filter 'action:not vs*'
        buildoptions { '-std=c++0x' }
        
    filter "platforms:x86"
        architecture "x86"
    filter "platforms:x64"
        architecture "x86_64"

    project 'wm_project'
        language 'C++'
        kind 'ConsoleApp'
        location 'build/%{_ACTION}/wm_project'

        files {
            'source/**'
        }

        filter 'release'
            links {
                'opencv_world310',
                'dlib'
            }
        filter 'debug'
            links {
                'opencv_world310d',
                'dlib'
            }
        filter '*'

        local libs = {
            'dlib',
            'tclap',
            'opencv',
            'glm'
        }
        for i = 1, #libs do
            includedirs { '3rdparty/'..libs[i]..'/include' }
        end
        if _ACTION and string.find(_ACTION, 'vs*') then
            local path = 'PATH=%PATH%;'
            for i = 1, #libs do
                libdirs { '3rdparty/'..libs[i]..'/%{cfg.platform}/%{_ACTION}/%{cfg.buildcfg}/lib' }
                path = path..'../../../3rdparty/'..libs[i]..'/%{cfg.platform}/%{_ACTION}/%{cfg.buildcfg}/bin;'
            end
            path = path..'../../../3rdparty/opencv/%{cfg.platform}/vs/any/bin;'
            debugenvs { path }
        else
            for i = 1, #libs do
                libdirs { '3rdparty/'..libs[i]..'/%{cfg.platform}/%{_ACTION}/lib' }
            end
        end
